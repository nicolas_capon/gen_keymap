# Keyboard Agnostic QMK Toolkit
Tool for centralizing keymap between QMK powered keyboards

Go into qmk_firmware directory and clone the repo
```bash
cd qmk_firmware && git clone https://gitlab.com/nicolas_capon/gen_keymap && cd gen_keymap
```

Basic usage:
```bash
python gen_keymap.py -f -kb ergodox_ez -km nico
```
This will automatically build the keymap in the right directory and run the qmk flash command on selected keyboard.

## Requirements

- [QMK](https://docs.qmk.fm/#/newbs_getting_started) installed
- [Python](https://www.python.org/downloads/) --version >= 3.6

## Documentation

### Arguments

To run the script use command like:
```bash
python gen_keymap.py --flash --keyboard <my_keyboard> --keymap <my_keymap> --no_key KC_TRANSPARENT
```

- --keyboard, -kb: keyboard name (used to find the corresponding directory)
- --keymap, -km: name of the directory to output the files (usually in qmk_firmware/keyboards/<keyboard_name>/keymaps/<keymap_name>/)
- --flash, -f: flag to automatically run the qmk flash command afterwards
- --compile, -c: flag to automatically run the qmk compile command afterwards
- --no_key: specify a default keycode for false values in keymap mask (default is KC_NO)

### Config.json

This file is found in the keyboards/<keyboard_name> folder.
It describes all specific parameters of this particular keyboard:
- name: name of the keyboard
- rules.mk: list of all rules to set to "yes" in the final rules.mk file
- layout_function: the qmk LAYOUT_ function to render the keymap (e.g., LAYOUT_ergodox_pretty or LAYOUT_all...)
- keymap: the mask to combine with the shared keymap to make the final one

The keymap mask is filled with 3 types of values:
- **String**: specify a keycode directly
- **Boolean**: If true, take the value of the shared keymap.
  First true value take the first shared keymap value and so on...
- **Integer**: Take the nth value of shared keymap (useful for keyboards with differents shapes)

To create a new keyboard specific keymap create a folder in keyboards directory with the same name as in qmk_firmware/keyboards/ 
Then create a new config.json file.

### Share directory

This directory is use to put all common informations between your keyboards (e.g. the relative keycodes positions, some rules.mk...)
Rules and keymap are defined in keymap.json

- rules is a list that combined shared and specific rules and set them to be equal to "yes" in a rules.mk file
- In the keymap.c file, the script search for this specific comment: /\* \_\_keymap\_\_ \*/ and replace it with the generated keymap
- The config.h file is simply copied to the destination folder.

*Using ifdef statements is useful to set specific blocks of code for keyboards with the desired rules defined.*

## TODO

- [ ] Use qmk configured keyboard and user as default value for --keymap and --keyboard ?
- [ ] Handle the case where a shared keymap layer is bigger than the keyboard mask
- [ ] Write tests
