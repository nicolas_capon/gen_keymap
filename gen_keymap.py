import os
import json
import argparse
from typing import List


def apply_mask(layer: list, mask: list, def_key: str) -> List[str]:
    """
    Create a keymap layer (list of keycodes) from:
    - a keyboard specific mask
    - a shared keymap layer
    - default_key (generally KC_NO) as def_key

    TODO: handle case if the shared keymap layer is bigger than the mask 
    """
    layer_result = []
    n = 0
    for ind, key in enumerate(mask):
        key_result = def_key
        key_type = type(key)
        if key_type is str:
            # We use this string as keycode
            key_result = key
        elif layer and key is True:
            # Take the nth value of the layer
            # where n is the number of time we encounter True in mask
            key_result = layer[n]
            n += 1
        elif layer and key_type is int and key < len(layer):
            # Take given int as keycode index in the layer
            key_result = layer[key]
        elif layer and key is not False:
            # Out of bound int
            print(f"Out of bound error with key {key}, max is {len(layer)-1}")
        layer_result.append(key_result)
    return layer_result

# Parsing
parser = argparse.ArgumentParser(description="Process user inputs.")

parser.add_argument("-km", "--keymap", help="keyboard Agnostic keymap in JSON format")
parser.add_argument("-kb", "--keyboard", help="Keyboard mask JSON formatted")
parser.add_argument("--no_key", default="KC_NO", type=str, help="")
parser.add_argument("-c", "--compile", nargs="?", const=True)
parser.add_argument("-f", "--flash", nargs="?", const=True)
args = parser.parse_args()

# Args
keyboard = args.keyboard
keymap = args.keymap
no_key = args.no_key

# handle destination directory
parent_dir = os.path.dirname(os.getcwd())
dest = os.path.join(parent_dir, "keyboards", keyboard, "keymaps", keymap)
if not os.path.isdir(dest):
    os.mkdir(dest)

# Load shared keyboard files
with open(os.path.join("share", "keymap.json"), "r") as file:
    keymap_config = json.load(file)
with open(os.path.join("share", "keymap.c"), "r") as file:
    keymap_c = file.read()

# Load specific keyboard config file
with open(os.path.join("keyboards", keyboard, f"config.json"), "r") as file:
    keyboard_config = json.load(file)

# Construct the final keymap
keymap_result = []
max_size_shared = len(keymap_config["keymap"])
max_size_mask = len(keyboard_config["keymap"])
max_len = max(max_size_shared, max_size_mask)
# For each layer, apply the shared keymap
# to the corresponding keyboard layer if there is one, otherwise: 
# - if there is no layer, apply no_key to all non string keycodes in the keyboard specific map
# - if there is no mask, apply the first layer mask
for ind in range(max_len):
    mask = keyboard_config["keymap"][0]
    layer = None
    if ind < len(keyboard_config["keymap"]):
        mask = keyboard_config["keymap"][ind]
    if ind < len(keymap_config["keymap"]):
        layer = keymap_config["keymap"][ind]
    keymap_result.append(apply_mask(layer, mask, no_key))

# Construct the final parameters
keyboard_layout_function = keyboard_config["layout_function"]
layers = [f"[{ind}] = {keyboard_layout_function}(" + ",".join(layer) + ")" for ind, layer in enumerate(keymap_result)]
result_str = "const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {\n" + ",\n".join(layers) + "\n};"
out = keymap_c.replace("/* __keymap__ */", result_str)
with open(os.path.join(dest, "keymap.c"), "w") as f:
    f.write(out)

# Create specific rules.mk file
rules = keyboard_config["rules.mk"] + keymap_config["rules.mk"]
with open(os.path.join(dest, "rules.mk"), "w") as f:
    f.write("\n".join([f"{rule} = yes" for rule in rules]))

# Copy config.h file to the destination
with open(os.path.join("share", "config.h"), "r") as file:
    config_h = file.read()
with open(os.path.join(dest, "config.h"), "w") as f:
    f.write(config_h)

# Display result
colors = {"green":"\033[1;32m", "white": "\033[1;37m", "yellow": "\033[1;33m"}
print(f"{colors['green']}Build successful!{colors['white']}\nKeymap generated in {dest}\n")
if args.flash:
    cmd = f"qmk flash -kb {keyboard} -km {keymap}"
    print(f"Executing following command\n{colors['yellow']}{cmd}{colors['white']}\n")
    os.system(cmd)
elif args.compile:
    cmd = f"qmk compile -kb {keyboard} -km {keymap}"
    print(f"Executing following command\n{colors['yellow']}{cmd}{colors['white']}\n")
    os.system(cmd)
else:
    print(f"To flash it, type this command:\n{colors['yellow']}qmk flash -kb {keyboard} -km {keymap}{colors['white']}")
